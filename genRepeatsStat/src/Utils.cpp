#include "Utils.h"
#include "SamFile.h" 
#include <string>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include <iostream>
#include "ErrorMatrixBuilder.h"

using namespace std;
static const int LOWEST_POSSIBLE_AS = -1000;
static const double Y_THRESHOLD_MM = 0.97;
static const double THRESHOLD_MM = 0.95;


// trim from start
static inline std::string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
        return ltrim(rtrim(s));
}

std::string Utils::normalizeHorName(const std::string& horName) {
	std::string chPart = horName.substr(horName.find("ch")+2);
	std::string result = chPart.substr(0, chPart.find("_"));

	return trim(result);
};
 

void Utils::printCoverage(const std::vector<std::string>& hors, const std::string& bamFile) {
	ErrorMatrixBuilder builder; 
	Matrix matrix(hors);
	builder.fillMatrix(matrix, bamFile);	

	std::vector<int> stat = matrix.diag();
 
	for (int i = 0; i < (int) stat.size(); ++i) {
		std::cout << matrix.getNameByIndex(i) << "\t" << stat[i] << std::endl;
	}   
}  