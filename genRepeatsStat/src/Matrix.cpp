#include "Matrix.h"
#include <algorithm>
#include <sstream>
#include <assert.h>
#include "Utils.h"
#include <iostream>

Matrix::Matrix(const std::vector<std::string>& names) {
	 _names = names;
  _mat_sz = names.size();
  _mat = new int*[_mat_sz];
  for (int i = 0; i < _mat_sz; ++i) { 
    _mat[i] = new int[_mat_sz];
  }
	
}

Matrix::~Matrix() {
  for (int i = 0; i < _mat_sz; ++i)
    delete[] _mat[i];
  delete[] _mat;
}
 

int Matrix::getIndexByName(const std::string& name)  {
  std::vector< std::string>::iterator  it;
  it = std::find(_names.begin(), _names.end(), Utils::normalizeHorName(name));

  return (it != _names.end())
         ? it - _names.begin()
         : -1;
}

const std::string& Matrix::getNameByIndex(int i) {
  return _names[i];
}

void Matrix::incValue(const std::string& name1, const std::string& name2) {
  int i = getIndexByName(name1);
  int j = getIndexByName(name2);

  if (i == -1) std::cout << name1 << std::endl;
  assert(i != -1);
  if (j == -1) std::cout << name2 << std::endl;
  assert(j != -1);

  incValue(i, j);
}

void Matrix::incValue(int i, int j) {
   _mat[i][j] ++;
   if (i != j)
    _mat[j][i] ++;
}

std::vector<int> Matrix::diag() {
  std::vector<int> stat;
  for (int i = 0; i <(int) _names.size(); ++i) { 
    stat.push_back(_mat[i][i]);
  }
  return stat;
}

std::string Matrix::toCsvString() {
  std::stringstream buffer;
  std::string delim(",");
  buffer << delim;
  for (int i = 0; i <(int) _names.size(); ++i)
        buffer << _names[i] << delim;
  buffer << std::endl;

  for (int i = 0; i <(int) _names.size(); ++i) {
    buffer << _names[i]<< delim;
       for (int j = 0; j <(int) _names.size(); ++j)
         buffer << _mat[i][j] << delim;
    buffer << std::endl;
  }
  return buffer.str();
}
