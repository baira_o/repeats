#include "Utils.h"
#include "Matrix.h"
#include "ErrorMatrixBuilder.h"

#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <ctype.h>
#include <string>
 
const char* hors[] = {
   	"1", "2", "3", "4", "5&19",
    "6", "7", "8", "9", "10", 
    "11", "12", "13&21", "14&22",
    "15", "16", "17", "18", "20", "X", "Y" 
};

template<typename T, size_t N>
T * end(T (&ra)[N]) {
    return ra + N;
}

void print_help(const std::string& program){
	std::cout << "Usage: " << program << " command [options]" << std::endl;
	std::cout << "\tExample of usage: " << program << " build-errmat -i bamFile" << std::endl;
}
void print_build_errmat_usage(const std::string& program) {
	std::cout << "Usage: " << program << " build-errmat -i bamFile [options]" << std::endl;
	std::cout << "\tExample of usage: " << program << " build-errmat -i bamFile" << std::endl;
	std::cout << "\tBuilds error matrix and prints it to output" << std::endl;
}

void print_name_usage(const std::string& program) {
	std::cout << "Usage: " << program << " name J1J2_ch9" << std::endl;
	std::cout << "Expected output: 9" << std::endl;
}

int main(int argc, char* argv[]) {
	std::vector<std::string> names(hors, end(hors));
	std::string bamFile; 
	int option; 
    
	if (argc > 1)  {
		std::string command(argv[1]);
		if (command == "build-errmat") { 
			
			while ((option = getopt(argc, argv, "i:")) != -1) {
	    		switch (option) { 
		         	case 'i' : 
		         		bamFile = optarg;
		            	break;   
		         	default: 
		         		print_build_errmat_usage(std::string(argv[0]));
		            	return 1;
	    		}	
			}		

    		if (bamFile.size() < 2) { // required
         		print_build_errmat_usage(std::string(argv[0]));
            	return 1;	    			
    		}	 
    		ErrorMatrixBuilder builder; 
			Matrix matrix(names);
			builder.fillMatrix(matrix, bamFile);	 
			std::cout << matrix.toCsvString();    	
			return 0;
		}
		else if (command == "name") {
			if (argc != 3) {
				print_name_usage(std::string(argv[0]));
				return 1;
			}
			std::cout << Utils::normalizeHorName(argv[2]) << std::endl;
			return 0;
		} 
		else if (command == "coverage") { 
			while ((option = getopt(argc, argv, "i:")) != -1) {
	    		switch (option) { 
		         	case 'i' : 
		         		bamFile = optarg;
		            	break;    
		         	default: 
		         		print_help(std::string(argv[0]));
		            	return 1;
	    		}	
			}	
    		if (bamFile.size() < 2) { // required
         		print_help(std::string(argv[0]));
            	return 1;	    			
    		}	 
 
    		Utils::printCoverage(names, bamFile);
    		return 0;
		} 
	}
	print_help(std::string(argv[0]));
	return 1;
}
