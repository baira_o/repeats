#include "ErrorMatrixBuilder.h"
#include "SamFile.h"
#include "Utils.h"
#include <algorithm>
#include <vector>
#include <string>
#include <map>

using namespace std;
static const double Y_THRESHOLD_MM = 0.97;
static const double THRESHOLD_MM = 0.95;
static const int MIN_AS = -100000;

ErrorMatrixBuilder::ErrorMatrixBuilder(){
}


void ErrorMatrixBuilder::fillMatrix(Matrix& matrix, std::string bamFile) {
        SamFile samIn;
        samIn.OpenForRead(bamFile.c_str());

        SamFileHeader samHeader;
        samIn.ReadHeader(samHeader);
        SamRecord samRecord;

        samIn.setSortedValidation(SamFile::QUERY_NAME);

        string lastReadName;
        int bestAlignmentScore = MIN_AS;
        map<string, int> alignments;

        while(samIn.ReadRecord(samHeader, samRecord))
        {
                int isFirstMate = (samRecord.getFlag() & 0x40) == 0x40;

                if (isFirstMate) {
                        string readName(samRecord.getReadName());
                        string refName(samRecord.getReferenceName());

                        string mateRefName = string(samRecord.getMateReferenceNameOrEqual());
                        bool isMateAlignedOnSameHor = (mateRefName == "=");

                        int score = MIN_AS; samRecord.getIntegerTag("AS", score);
                        int mateScore = MIN_AS; samRecord.getIntegerTag("YS", mateScore);
                        int pairScore = score + mateScore;

                        if ((readName != lastReadName) && (lastReadName.size() > 0) && (alignments.size() > 0))
                        {
                                vector<string> horNames;
                                map<string, int>::iterator it;
                                for (it=alignments.begin(); it!=alignments.end(); ++it) {
                                        if (it->second == bestAlignmentScore) {
                                                horNames.push_back(it->first);
                                        }
                                }
                                for (int i = 0; i < (int) horNames.size(); ++i)
                                for (int j = i; j < (int) horNames.size(); ++j)
                                        matrix.incValue(horNames[i], horNames[j]);

                                alignments.clear();
                                bestAlignmentScore = pairScore;
                        }

                        int mismatchCount = 0; samRecord.getIntegerTag("NM", mismatchCount);

                        double similarity = 1 - (double) mismatchCount / samRecord.getReadLength();
                        double similarity_thr = Utils::normalizeHorName(refName) == "Y"
                                ? Y_THRESHOLD_MM
                                : THRESHOLD_MM;

                        if ((similarity >= similarity_thr) &&
                                isMateAlignedOnSameHor
                                && (pairScore >= bestAlignmentScore)) {
                                map<string, int>::iterator alignmentIter = alignments.find(refName);

                                if (alignmentIter == alignments.end())
                                {
                                        pair<string, int> alignment(refName, pairScore);
                                        alignments.insert(alignment);
                                }
                                else if (alignmentIter->second < pairScore)     {
                                                alignmentIter->second = pairScore;
                                }
                                bestAlignmentScore = pairScore;
                        }
                        lastReadName = readName;
                }
        }

        if (alignments.size() > 0)
        {
                vector<string> horNames;
                map<string, int>::iterator it;
                for (it=alignments.begin(); it!=alignments.end(); ++it) {
                        if (it->second == bestAlignmentScore) {
                                horNames.push_back(it->first);
                        }
                }
                for (int i = 0; i < (int) horNames.size(); ++i)
                for (int j = i; j < (int) horNames.size(); ++j)
                        matrix.incValue(horNames[i], horNames[j]);

                alignments.clear();
        }

}
