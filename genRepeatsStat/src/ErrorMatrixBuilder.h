#ifndef _ERROR_MATRIX_BUILDER_H_
#define _ERROR_MATRIX_BUILDER_H_

#include "Matrix.h"
#include <string>
#include <fstream>
#include <iostream>

class ErrorMatrixBuilder {
public:
	ErrorMatrixBuilder();
 
	void fillMatrix(Matrix& matrix, std::string bamFile); 
};	

#endif
