#ifndef _UTILS_H_
#define _UITLS_H_
#include <string>
#include <vector>

namespace Utils {
	std::string normalizeHorName(const std::string& horName); 
	void printCoverage(const std::vector<std::string>& hors, const std::string& bamFile);
};

#endif
