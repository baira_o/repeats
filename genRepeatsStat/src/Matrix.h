#ifndef _MATRIX_H_
#define _MATRIX_H_

#include <string>
#include <vector>

class Matrix
{
public:
    Matrix(const std::vector<std::string>& names);
    ~Matrix(); //todo: copy constructor, =operator

    int getIndexByName(const std::string& name);
    const std::string& getNameByIndex(int i) ;

    void incValue(const std::string& name1, const std::string& name2);
    void incValue(int i, int j);

    std::vector<int> diag();
    std::string toCsvString() ;
private:
    int** _mat;
    int _mat_sz;
    std::vector<std::string> _names;
};

#endif
